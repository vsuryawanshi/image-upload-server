var createError = require('http-errors');
var express = require('express');
var path = require('path');
var f = require("util").format;
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var db = require('./db')

const formidable = require('express-formidable');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

var user = encodeURIComponent('varun');
var password = encodeURIComponent('var@woocation7#*');

var url = f('mongodb://%s:%s@localhost:27017/smsdb?authSource=admin',user,password);

var localurl = f('mongodb://localhost:27017/smsdb');
db.connect(url, function (err) {
	if (err) {
		console.log('Unable to connect to Mongo.')
		process.exit(1)
	} else {
		console.log('Listening on port 2000...')
	}
})

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(formidable());

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
