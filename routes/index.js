var express = require('express');
var router = express.Router();
var db = require("../db");
var ObjectID = require('mongodb').ObjectID;
const fs = require("fs");
var os = require("os");
var ip = require('ip');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send("Upload Image server");
});


router.post("/upload", (req,res) => {
    var type = req.fields.type;
    var image = req.files.image;

    console.log(ip.address());

    var fileName = "image_" + (new Date).getTime();
    var tmp_path = image.path;
    var ext = image.name.substring(image.name.lastIndexOf("."));
    var target_path = './public/images/' + fileName + "" + ext;
    var urlToSaveInDb = req.protocol + "://" + ip.address() + ":" + req.app.settings.port + "/images/" + fileName  + "" + ext;
	var userName = req.fields.name;
    fs.rename(tmp_path, target_path, function(err) {
      if (err) throw err;
      
      var collection = db.get().db().collection("appimages");
      collection.insertOne({type:type,image:urlToSaveInDb,name:userName}, function(err,response){
          if(err){
              res.status(500).send(err);
          } else {
              res.send("success");
          }
      }) 
  });
})


module.exports = router;
